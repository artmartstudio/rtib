const sharp = require("sharp");
const Canvas = require('canvas');

var fileName = "/home/cpamx/Work/projects/mp/mp-rtib-js/test.jpg";

const image = sharp(fileName);

const roundRect = (ctx, x, y, width, height, radius) => {
  radius = {tl: radius, tr: radius, br: radius, bl: radius};
  ctx.beginPath();
  ctx.moveTo(x + radius.tl, y);
  ctx.lineTo(x + width - radius.tr, y);
  ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
  ctx.lineTo(x + width, y + height - radius.br);
  ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
  ctx.lineTo(x + radius.bl, y + height);
  ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
  ctx.lineTo(x, y + radius.tl);
  ctx.quadraticCurveTo(x, y, x + radius.tl, y);
  ctx.closePath();
  ctx.fill();
}

const getTextWidth = (text) => {
  const canvas = new Canvas(0, 0), ctx = canvas.getContext('2d');
  ctx.font = '16px Open Sans';
  return ctx.measureText(text).width;
};

const watermark = (text) => {
  const width = getTextWidth(text) + 110;
  const height = 30;
  var canvas = new Canvas(width, height), ctx = canvas.getContext('2d');
  ctx.font = '16px Open Sans';

  ctx.beginPath();
  const grd = ctx.createLinearGradient(0, 0, 0, height);
  grd.addColorStop(0, "rgba(43, 86, 159, 1)");
  grd.addColorStop(1, "rgba(23, 58, 140, 1)");
  ctx.fillStyle = grd;
  roundRect(ctx, 0, 0, width, height, 6);

  ctx.beginPath();
  ctx.moveTo(height, 0);
  ctx.lineTo(height, height);
  ctx.strokeStyle = "rgba(255, 255, 255, 0.5)";
  ctx.stroke();

  ctx.fillStyle = "rgba(255, 255, 255, 0.7)";
  ctx.fillText(text, ((width) / 2 - ((width - 110) / 2)) + (height / 2), 20);

  return {buffer: canvas.toBuffer(), width: width};
};

image
  .metadata()
  .then((metadata) => {
    const width = Math.round(metadata.height * 16 / 9);
    const height = metadata.height;
    
    const wt = watermark("mpn.ua/id444");

    sharp("/home/cpamx/Work/projects/mp/mp-rtib-js/test.jpg")
      .overlayWith(wt.buffer, { top: height - 40, left: width - wt.width - 10 })
      .toFile("/home/cpamx/Work/projects/mp/mp-rtib-js/output.jpg");
  });